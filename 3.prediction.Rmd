---
title: "Assignment 1 - Language Development in ASD - part 3"
author: "Kathrine, Signe & Frederikke"
date: "October 3rd, 2018"
output: word_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
#getwd()

```


## Welcome to the third exciting part of the Language Development in ASD exercise

In this exercise we will delve more in depth with different practices of model comparison and model selection, by first evaluating your models from last time, then learning how to cross-validate models and finally how to systematically compare models.

N.B. There are several datasets for this exercise, so pay attention to which one you are using!

1. The (training) dataset from last time (the awesome one you produced :-) ).
2. The (test) datasets on which you can test the models from last time:

```{r setup, include=FALSE}

library(tidyverse)
library(ggplot2)
library(lme4)
library(Metrics)
library(dplyr)
library(stringr)
#packageVersion("dplyr")

#loading data created in assignment 1
data <- read.csv("1.final_language_data.csv",header=T, sep=",")

#new datasets
demo_test <- read.csv("demo_test.csv",header=T, sep=",")

ul_test <- read.csv("LU_test.csv" ,header=T, sep=",")

toke_test <- read.csv("token_test.csv",header=T, sep=",")

```


### Exercise 1) Testing model performance

How did your models from last time perform? In this exercise you have to compare the results on the training data () and on the test data. Report both of them. Compare them. Discuss why they are different.

- recreate the models you chose last time (just write the model code again and apply it to your training data (from the first assignment))
- calculate performance of the model on the training data: root mean square error is a good measure. (Tip: google the function rmse())
- create the test dataset (apply the code from assignment 1 part 1 to clean up the 3 test datasets)
- test the performance of the models on the test data (Tips: google the functions "predict()")
- optional: predictions are never certain, can you identify the uncertainty of the predictions? (e.g. google predictinterval())

formatting tip: If you write code in this document and plan to hand it in, remember to put include=FALSE in the code chunks before handing in.

```{r setup, include=FALSE}

#best model from assignment 1 part 2 

mlu_model_3 <- lmer(c_mlu~visit*diagnosis+Age+gender+(1+visit|subject), data, REML = F)
summary(mlu_model_3)


# calculate performance of the model on training data 
# use rmse() which is the root mean square error 

predicted <- predict(mlu_model_3, data)

rmse <- rmse(data$c_mlu, predicted)
rmse



```



```{r setup, include=FALSE}

# cleaning up the test data sets into one test data set
# all copied from assigment part 1 


# We transform variable names in each dataset using dplyr (within tidyverse)

#Demographic data for the participants:
demo_test <- dplyr::rename(demo_test, visit=Visit)

demo_test <- dplyr::rename(demo_test, subject=Child.ID, nonVerbalIQ=MullenRaw, verbalIQ=ExpressiveLangRaw)

#Word data:
token_test<-dplyr::rename(toke_test, subject=SUBJ, visit=VISIT, m_uniqw=types_MOT, c_uniqw=types_CHI, shared_uniqw=types_shared, c_words=tokens_CHI, m_words=tokens_MOT)

#Deleting the last and unnecessary column called X

token_test<-select(token_test,-X)

#Length of utterance data:
utter_test<-dplyr::rename(ul_test, subject=SUBJ,visit=VISIT, m_mlu=MOT_MLU, m_lu_std=MOT_LUstd, m_lu_q1=MOT_LU_q1,m_lu_q2=MOT_LU_q2,m_lu_q3=MOT_LU_q3,c_mlu=CHI_MLU,c_lu_std=CHI_LUstd,c_lu_q1=CHI_LU_q1,c_lu_q2=CHI_LU_q2,c_lu_q3=CHI_LU_q3)




utter_test$visit <- str_extract(utter_test$visit, "[0-9]+")  

token_test$visit <- str_extract(token_test$visit, "[0-9]+")




utter_test$subject <- gsub("\\.", "", utter_test$subject)

token_test$subject <- gsub("\\.", "", token_test$subject)

demo_test$subject <- gsub("\\.", "", demo_test$subject)




demo_new1 <- select(demo_test, c(subject, visit, Ethnicity, Diagnosis, Gender, Age, ADOS, nonVerbalIQ, verbalIQ))

utter_new1 <- select(utter_test, c(subject, visit, m_mlu, m_lu_std, c_mlu, c_lu_std))

token_new1 <- select(token_test, c(subject, visit, m_uniqw, c_uniqw, m_words, c_words))




token_test <- plyr::join(token_new1, utter_new1)  


lan_data1 <- plyr::join(token_test, demo_new1, type="full")  





subset<-lan_data1 %>%
  filter(visit==1) %>%
  select(subject,ADOS)

#We create new variable names 
subset1 <- dplyr::rename(subset, ados=ADOS)

# We merge subset to lan_data 
final_data_test<- merge(lan_data1,subset1)

#We select which columns to keep and define the order of values
final_data_test <-select(final_data_test,subject,visit,Diagnosis,Age,Gender,Ethnicity,ados,c_uniqw,m_uniqw,c_words,m_words,c_mlu,m_mlu,c_lu_std,m_lu_std)




final_data_test<-final_data_test %>%
  mutate(subject = as.numeric(as.factor(subject)))

#We change the values "1" and "2" within the gender column into "F" and "M"
final_data_test <- final_data_test %>%
mutate(gender = ifelse(Gender %in% c("1"), "M", "F"))

#We delete the old column 
final_data_test$Gender <- NULL #deletes the old column

#We change the values "1" and "2" within the diagnosis column into "ASD" and "TD"
final_data_test <- final_data_test %>%
mutate(diagnosis = ifelse(Diagnosis %in% c("A"), "ASD", "TD"))

#We place the columns in the right order
final_data_test <-select(final_data_test,subject,visit,diagnosis,Age,gender,Ethnicity,ados,c_uniqw,m_uniqw,c_words,m_words,c_mlu,m_mlu,c_lu_std,m_lu_std)


#We write the data into a csv file
write.csv(final_data_test, file = "language_data_test.csv")


```



```{r}
predicted1 <- predict(mlu_model_3, final_data_test)

rmse1 <- rmse(data$c_mlu, predicted)

allow.new.levels=T
rmse
```

[HERE GOES YOUR ANSWER]

### Exercise 2) Model Selection via Cross-validation (N.B: ChildMLU!)

One way to reduce bad surprises when testing a model on new data is to train the model via cross-validation. 

In this exercise you have to use cross-validation to calculate the predictive error of your models and use this predictive error to select the best possible model.

- Use cross-validation to compare your model from last week with the basic model (Child MLU as a function of Time and Diagnosis, and don't forget the random effects!)
- (Tips): google the function "createFolds";  loop through each fold, train both models on the other folds and test them on the fold)

Which model is better at predicting new data: the one you selected last week or the one chosen via cross-validation this week?

- Test both of them on the test data.
- Report the results and comment on them.

- Now try to find the best possible predictive model of ChildMLU, that is, the one that produces the best cross-validated results.

- Bonus Question 1: What is the effect of changing the number of folds? Can you plot RMSE as a function of number of folds?
- Bonus Question 2: compare the cross-validated predictive error against the actual predictive error on the test data

```{r}


# create a "for" loop 
#for (F in folds){
  
} 
#take all the kinds that are/are not in the test fold
#like the video 



```


[HERE GOES YOUR ANSWER]

### Exercise 3) Assessing the single child

Let's get to business. This new kiddo - Bernie - has entered your clinic. This child has to be assessed according to his group's average and his expected development.

Bernie is one of the six kids in the test dataset, so make sure to extract that child alone for the following analysis.

You want to evaluate:

- how does the child fare in ChildMLU compared to the average TD child at each visit? Define the distance in terms of absolute difference between this Child and the average TD.
(Tip: recreate the equation of the model: Y=Intercept+BetaX1+BetaX2, etc; input the average of the TD group  for each parameter in the model as X1, X2, etc.).

- how does the child fare compared to the model predictions at Visit 6? Is the child below or above expectations? (tip: use the predict() function on Bernie's data only and compare the prediction with the actual performance of the child)

[HERE GOES YOUR ANSWER]

### OPTIONAL: Exercise 4) Model Selection via Information Criteria
Another way to reduce the bad surprises when testing a model on new data is to pay close attention to the relative information criteria between the models you are comparing. Let's learn how to do that!

Re-create a selection of possible models explaining ChildMLU (the ones you tested for exercise 2, but now trained on the full dataset and not cross-validated).

Then try to find the best possible predictive model of ChildMLU, that is, the one that produces the lowest information criterion.

- Bonus question for the optional exercise: are information criteria correlated with cross-validated RMSE? That is, if you take AIC for Model 1, Model 2 and Model 3, do they co-vary with their cross-validated RMSE?

### OPTIONAL: Exercise 5): Using Lasso for model selection

Welcome to the last secret exercise. If you have already solved the previous exercises, and still there's not enough for you, you can expand your expertise by learning about penalizations. Check out this tutorial: http://machinelearningmastery.com/penalized-regression-in-r/ and make sure to google what penalization is, with a focus on L1 and L2-norms. Then try them on your data!

