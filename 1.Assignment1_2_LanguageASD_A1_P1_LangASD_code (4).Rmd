---
title: "A1_P1_Student"
author: "Riccardo Fusaroli"
date: "01/07/2018"
output: html_document
---

Kan i se denne ændring online?

# Assignment 1, Part 1: Language development in Autism Spectrum Disorder (ASD) - Brushing up your code skills

In this first part of the assignment we will brush up your programming skills, and make you familiar with the data sets you will be analysing for the next parts of the assignment.

In this first part of the assignment you will:
1) Create a Github (or gitlab) account and link it to your RStudio
2) Use small nifty lines of code to transform several data sets into just one. The final data set will contain only the variables that are needed for the analysis in the next parts of the assignment
3) Become familiar with the tidyverse package (especially the sub-packages stringr and dplyr), which you will find handy for later assignments.


## 0. First an introduction on the data

# Language development in Autism Spectrum Disorder (ASD)

Background: Autism Spectrum Disorder is often related to language impairment. However, this phenomenon has not been empirically traced in detail: i) relying on actual naturalistic language production, ii) over extended periods of time. We therefore videotaped circa 30 kids with ASD and circa 30 comparison kids (matched by linguistic performance at visit 1) for ca. 30 minutes of naturalistic interactions with a parent. We repeated the data collection 6 times per kid, with 4 months between each visit. We transcribed the data and counted: 
i) the amount of words that each kid uses in each video. Same for the parent.
ii) the amount of unique words that each kid uses in each video. Same for the parent.
iii) the amount of morphemes per utterance (Mean Length of Utterance) displayed by each child in each video. Same for the parent. 

## 1. Let's get started on GitHub

Follow the link to a Github tutorial: 
https://support.rstudio.com/hc/en-us/articles/200532077-Version-Control-with-Git-and-SVN

In the assignments you will be asked to upload your code on Github and the GitHub repositories will be part of the portfolio, therefore all students must make an account and link it to their RStudio (you'll thank us later for this!).

N.B. Create a GitHub repository for the Language Development in ASD set of assignments and link it to a project on your RStudio.

You may also use Gitlab instead of Github (Malte will explain in class)

## 2. Now let's take dirty dirty data sets and make them into a tidy one

If you're not in a project in Rstudio, make sure to set your working directory here.
If you created an rstudio project, then your working directory (the directory with your data and code for these assignments) is the project directory. You can always see which working directory is used with `getwd()`. Note that this may be different in the console than in the Rmd cells (don't ask me why).

```{r}
#making sure the WD is set to the right project
getwd()
```

Load the three data sets, after downloading them from dropbox and saving them in your working directory:
* Demographic data for the participants: https://www.dropbox.com/s/w15pou9wstgc8fe/demo_train.csv?dl=0
* Length of utterance data: https://www.dropbox.com/s/usyauqm37a76of6/LU_train.csv?dl=0
* Word data: https://www.dropbox.com/s/8ng1civpl2aux58/token_train.csv?dl=0

```{r}

##downoading the datasets 

#demo

demo <- read.csv(file="C:\\Users\\Frederikke\\OneDrive\\cognitive_science\\3rd. Semester\\Experimental Methods 3\\1_language\\demo_train.csv", header=TRUE, sep=",")


#utterancelength

utter<- read.csv(file="C:\\Users\\Frederikke\\OneDrive\\cognitive_science\\3rd. Semester\\Experimental Methods 3\\1_language\\LU_train.csv", header=TRUE, sep=",")

#words

words<-read.csv(file="C:\\Users\\Frederikke\\OneDrive\\cognitive_science\\3rd. Semester\\Experimental Methods 3\\1_language\\token_train.csv", header=TRUE, sep=",")

```

Explore the 3 datasets (e.g. visualize them, summarize them, etc.). You will see that the data is messy, since the psychologists collected the demographic data, a linguist analyzed the length of utterance in May 2014 and the same linguist analyzed the words several months later. In particular:
- the same variables might have different names (e.g. identifier of the child)
- the same variables might report the values in different ways (e.g. visit)
Welcome to real world of messy data :-)

Before being able to combine the data sets we need to make sure the relevant variables have the same names and the same kind of values.

So:

2a. Find a way to transform variable names.
Tip: Look into the package dplyr (part of tidyverse)
Tip: Or google "how to rename variables in R". 
Tip: Or look through the chapter on data transformation in R for data science (http://r4ds.had.co.nz).


```{r}
##Transform variable names using dplyr (within tidyverse)

#alternative in basic R: names(utter)[names(utter) == "SUBJ"] <- "subject"   (names is the name of the function)

library(tidyverse)
library(dplyr)
library(stringr)


packageVersion("dplyr")


###demo

demo <- dplyr::rename(demo, visit=Visit)

demo <- dplyr::rename(demo, subject=Child.ID, nonVerbalIQ=MullenRaw, verbalIQ=ExpressiveLangRaw)




###words

words<-dplyr::rename(words, subject=SUBJ)

words<-dplyr::rename(words, visit=VISIT)

words<-dplyr::rename(words, m_uniqw=types_MOT)

words<-dplyr::rename(words, c_uniqw=types_CHI)

words<-dplyr::rename(words, shared_uniqw=types_shared)

words<-dplyr::rename(words, c_words=tokens_CHI, m_words=tokens_MOT)

#deleting the last and unnecessary column called X

words<-select(words,-X)


###utter

utter<-dplyr::rename(utter, subject=SUBJ,visit=VISIT, m_mlu=MOT_MLU, m_lu_std=MOT_LUstd, m_lu_q1=MOT_LU_q1,m_lu_q2=MOT_LU_q2,m_lu_q3=MOT_LU_q3,c_mlu=CHI_MLU,c_lu_std=CHI_LUstd,c_lu_q1=CHI_LU_q1,c_lu_q2=CHI_LU_q2,c_lu_q3=CHI_LU_q3)


### OTHER SOLUTION

#data<-rename.vars(data, "OLD", "New") ?Possible 
#COLNAMES(data) = ....

```

2b. Find a way to homogeneize the way "visit" is reported. If you look into the original data sets, you will see that in the LU data and the Token data, Visits are called "visit 1"" instead of just "1"" (which is the case in the demographic data set).
Tip: There is a package called stringr, which will be very handy for manipulating (text) strings also in furture assignments. We will return to this package later, but for now use the str_extract () to extract only the number from the variable Visit in each data set.

```{r}

###homogenizie visits in the df utter and the df words

utter$visit <- str_extract(utter$visit, "[0-9]+") ###[:digit:] could do this too. Squarebrackets say find something that matches what is in here 
#str_extract(data$col, "\\d") might also have worked (according to Malte)

words$visit <- str_extract(words$visit, "[0-9]+")


### OTHER SOLUTIONS
#as.numeric (Can this work?)





```

2c. We also need to make a small adjustment to the content of the Child.ID coloumn in the demographic data. Within this column, names that are not abbreviations do not end with "." (i.e. Adam), which is the case in the other two data sets (i.e. Adam.). If The content of the two variables isn't identical the data sets will not be merged sufficiently.
We wish to remove the "." at the end of names in the LU data and the tokens data.

Tip: stringr is helpful again. Look up str_replace_all
Tip: You can either have one line of code for each child name that is to be changed (easier, more typing) or specify the pattern that you want to match (more complicated: look up "regular expressions", but less typing)

Tip: You will have to do identical work for both data sets, so to save time on the copy/paste use the cmd+f/ctrl+f function. Add the data frame name (e.g. LU_data) in the first box, and the data frame name (e.g. Tokens_data) you wish to change it to in the other box, and press replace. Or create a function that takes the data set and does the transformation. Then call the function on both data sets.


```{r}

#LU = df utter 
#token = column "words"

#We removed all dots from the subject column, both in full names and in the abbreviations using gsub. gsub perform replacement of the first and all matches respectively, which makes all columns identical 

utter$subject <- gsub("\\.", "", utter$subject) #//. means remove all dots

words$subject <- gsub("\\.", "", words$subject)

demo$subject <- gsub("\\.", "", demo$subject)


### OTHER SOLUTIONS

# str_replace_all with [:punct:] might do this too 



```

2d. Now that the nitty gritty details of the different data sets are fixed, we want to make a subset of each data set only containig the variables that we wish to use in the final data set.
For this we use the tidyverse package dplyr, which contains the function select(). 

The variables we need are: Child.ID, Visit, Ethnicity, Diagnosis, Gender, Age, ADOS,  MullenRaw, ExpressiveLangRaw, MOT_MLU, MOT_LUstd, CHI_MLU, CHI_LUstd, types_MOT, types_CHI, tokens_MOT, tokens_CHI.

* ADOS indicates the severity of the autistic symptoms (the higher the score, the worse the symptoms)
* MLU stands for mean length of utterance
* types stands for unique words (e.g. even if "doggie" is used 100 times it only counts for 1)
* tokens stands for overall amount of words (if "doggie" is used 100 times it counts for 100) 
* MullenRaw indicates non verbal IQ
* ExpressiveLangRaw indicates verbal IQ

It would be smart to rename the last two into something you can remember (i.e. nonVerbalIQ, verbalIQ)

```{r}
#We changed the column names earlier, see above 
#Now we make a new df of each df in which we have only the necessary columns 

demo_new <- select(demo, c(subject, visit, Ethnicity, Diagnosis, Gender, Age, ADOS, nonVerbalIQ, verbalIQ))

utter_new <- select(utter, c(subject, visit, m_mlu, m_lu_std, c_mlu, c_lu_std))

words_new <- select(words, c(subject, visit, m_uniqw, c_uniqw, m_words, c_words))
```

2e. Finally we are ready to merge all the data sets into just one. 
Tip: Look up "joins" in R for data science, or google "How to merge datasets in R"
Tip: Joining / merging only works for two data frames at the time.

```{r}
#We create a contemporary df by the df subsets of words_new and utter_new

library(tidyverse)



words_utter <- plyr::join(words_new, utter_new)  #inner joining id default for merge. See R for datascience - joining to see how to join them (inner_join, right, left, full) full_join is default for join 


#We merge the contemporary df words_utter with the last df subset of demo into one large df named lan_data

lan_data <- plyr::join(words_utter, demo_new, type="full")  #full bc. we do not want to drop any of the data, so we will just have some NA's



```

Are we done?
If you look at the data set now, you'll se a lot of NA's in the variables ADOS, nonVerbalIQ (MullenRaw) and verbalIQ (ExpressiveLangRaw). These measures were not taken at all visits. Additionally, we only want these measures for the first visit (Riccardo will explain why in class).
So let's make sure that we select only these variables as collected during the first visit for each child and repeat these values throughout all other visits.

* A way to get around this in R, is to make a subset of the data containing only the values we are interested in, i.e. the values at visit 1. We only want to keep the relevant variables in this data set, i.e the ones which values are to be repeated. Either the the subset() function or select() and filter() can be used here. Solve this issue with both functions to familiarize yourself with these useful functions. N.B. save the subset of the dataset as a new dataset, do not overwrite the old one.

* In order to merge these new variables to the final data set, they'll need new names. E.g change the ADOS variable to ADOS1.

* Once you've changed the names, the subset can be merged to the final data set, and the score at the 1st visit in each variable will be spread to all 6.

* Lastly, there are too many unneccesary variables in the data set by now. Use the select() to choose only the variables you want in the data (e.g. remove the old ADOS, verbal and nonVerbal IQ variables, so you will not get confused later on) and define the order of the variables. Hint: You want the child identifier, followed by diagnosis, followed by demographic, cognitive and clinical features, followed by indexes of linguistic performance (utterances lenghts, types and tokens of words).


```{r}

#subset to have visit 1 (select, filter, subset) as new data 


subset<-lan_data %>%
  filter(visit==1) %>%
  select(subject,ADOS)

#make new variable names 




subset <- dplyr::rename(subset, ados=ADOS)


#merge to subset to lan_data 

final_data<- merge(lan_data,subset)

#select which columns to keep + deifne order of values

final_data <-select(final_data,subject,visit,Diagnosis,Age,Gender,Ethnicity,ados,c_uniqw,m_uniqw,c_words,m_words,c_mlu,m_mlu,c_lu_std,m_lu_std)



```

Now, we are almost ready to start analysing the data. However, here are some additional finishing touches:

* in some experiments your participants must be anonymous. Therefore we wish to turn the CHILD.ID into numbers.

Tip: You will probably need to turn it into a factor first, then a number
Tip: google "R how to convert character to integer" or look up the as.??? functions

* In order to make it easier to work with this nice, clean dataset in the future, it is practical to make sure the variables have sensible values. E.g. right now gender is marked 1 and 2, but in two weeks you will not be able to remember, which gender were connected to which number, so change the values from 1 and 2 to F and M in the gender variable. For the same reason, you should also change the values of Diagnosis from A and B to ASD (autism spectrum disorder) and TD (typically developing). Tip: Try taking a look at ifelse(), or google "how to rename levels in R".

```{r}
#turn subject into numbers 

final_data<-final_data %>%
  mutate(subject = as.numeric(as.factor(subject)))


#change gender from 1 and 2 to F or M

final_data <- final_data %>%
mutate(gender = ifelse(Gender %in% c("1"), "M", "F"))

#delete old column 
final_data$Gender <- NULL #deletes the old column


#Change diagnosis from 1 and 2 -> ASD and TD

final_data <- final_data %>%
mutate(diagnosis = ifelse(Diagnosis %in% c("A"), "ASD", "TD"))


## Make it pretty again, columns in the right order

final_data <-select(final_data,subject,visit,diagnosis,Age,gender,Ethnicity,ados,c_uniqw,m_uniqw,c_words,m_words,c_mlu,m_mlu,c_lu_std,m_lu_std)
```


Write the data set into a csv file. 

```{r}

write.csv(final_data, file = "language_data.csv")
```


3) Now that we have a nice clean data set to use for the analysis next week, we shall play a bit around with it. The following exercises are not relevant for the analysis, but are here so you can get familiar with the functions within the tidyverse package.

Here's the link to a very helpful book, which explains each function:
http://r4ds.had.co.nz/index.html

USING FILTER
List all kids who:
1. have a mean length of utterance (across all visits) of more than 2.7 morphemes.
2. have a mean length of utterance of less than 1.5 morphemes at the first visit
3. have not completed all trials. Tip: Use pipes `%>%` to solve this

```{r}

###1 

mlu_above_2.7<-filter(final_data, c_mlu>2.7) 

###2

mlu_less_1.5_visit1<-final_data %>%
  filter(c_mlu<1.5, visit==1)

##3

na<-final_data %>% 
  filter(!complete.cases(.)) # "!" reverses the operation of only keeping complete cases 


```


USING ARRANGE

1. Sort kids to find the kid who produced the most words on the 6th visit
2. Sort kids to find the kid who produced the least amount of words on the 1st visit.

```{r}

###1 

most_words_6<- final_data %>%
  filter(visit==6) %>%
  arrange(desc(c_words)) %>%
  slice(1) #head() might work, selects the first number of rows, depending on which number you put


###2

least_words_1<- final_data %>%
  filter(visit==1) %>%
  arrange(!desc(c_words)) %>%    #udråbstegn foran 
  slice(1)

```

USING SELECT

1. Make a subset of the data including only kids with ASD, mlu and word tokens
2. What happens if you include the name of a variable multiple times in a select() call?

```{r}

###1 

ASD<- final_data %>%
  filter(diagnosis=="ASD") %>%
  select(subject,c_mlu,c_words, diagnosis) 

###2

ASD1<- final_data %>%
  filter(diagnosis=="ASD") %>%
  select(subject,c_mlu,c_words, diagnosis, diagnosis) 

# double diagnosis -> it just takes one 



```


USING MUTATE, SUMMARISE and PIPES
1. Add a column to the data set that represents the mean number of words spoken during all visits.
2. Use the summarise function and pipes to add an column in the data set containing the mean amount of words produced by each trial across all visits. HINT: group by Child.ID 
3. The solution to task above enables us to assess the average amount of words produced by each child. Why don't we just use these average values to describe the language production of the children? What is the advantage of keeping all the data?

```{r}

###1 

final_data<-final_data %>% 
  filter(complete.cases(.)) %>%
    mutate(mean_words= mean(c_words))


###2

final_data<-final_data %>% 
  filter(complete.cases(.)) %>%
  group_by(subject) %>%
  dplyr::mutate(mean_words_individual= mean(c_words)) 

#saving as csv

write.csv(final_data, file = "1.final_language_data.csv")

###3

#Written response 





```
