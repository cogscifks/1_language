---
title: "Assignment 1 - Language Development in ASD - part 3"
author: "Riccardo Fusaroli"
date: "August 10, 2018"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Welcome to the third exciting part of the Language Development in ASD exercise

In this exercise we will delve more in depth with different practices of model comparison and model selection, by first evaluating your models from last time, then learning how to cross-validate models and finally how to systematically compare models.

N.B. There are several datasets for this exercise, so pay attention to which one you are using!

1. The (training) dataset from last time (the awesome one you produced :-) ).
2. The (test) datasets on which you can test the models from last time:
* Demographic and clinical data: https://www.dropbox.com/s/ra99bdvm6fzay3g/demo_test.csv?dl=1
* Utterance Length data: https://www.dropbox.com/s/uxtqqzl18nwxowq/LU_test.csv?dl=1
* Word data: https://www.dropbox.com/s/1ces4hv8kh0stov/token_test.csv?dl=1

```{r}

library(tidyverse)
library(pacman)
library(lme4)
library(lmerTest)
library(Matrix)
library(pacman)
library(caret)
#p_load(MuMIn)
p_load(ModelMetrics)
#p_load(caret)


#loading data created in assignment 1
data <- read.csv("1.final_language_data.csv",header=T, sep=",")

#deleting rows so they have the same dimentions 
data <- data[-c(1) ]

data <- data[-c(16) ]

data <- data[-c(16) ]


#reading the csv made in another script, where the new test data set is cleaned up and created
final_data_test <- read.csv("language_data_test.csv",header=T, sep=",")

#data_test
data_test <- final_data_test[-c(6), ]

data_test <- data_test[-c(1) ]

```


### Exercise 1) Testing model performance

How did your models from last time perform? In this exercise you have to compare the results on the training data () and on the test data. Report both of them. Compare them. Discuss why they are different.

- recreate the models you chose last time (just write the model code again and apply it to your training data (from the first assignment))
- calculate performance of the model on the training data: root mean square error is a good measure. (Tip: google the function rmse())
- create the test dataset (apply the code from assignment 1 part 1 to clean up the 3 test datasets)
- test the performance of the models on the test data (Tips: google the functions "predict()")
- optional: predictions are never certain, can you identify the uncertainty of the predictions? (e.g. google predictinterval())

formatting tip: If you write code in this document and plan to hand it in, remember to put include=FALSE in the code chunks before handing in.

```{r}

#calculate performance of the basic model and the best model on TRAINING data

#BASIC model 
mlu_basic <- lmer(c_mlu~Age+gender+(1+visit|subject), data, REML = F)

#best model from assignment 1 part 2 
mlu_model_3 <- lmer(c_mlu~visit*diagnosis+Age+gender+(1+visit|subject), data, REML = F)

#predict basic model
predict_basic_train <- predict(mlu_basic, data)

#testing performance of basic modelon training data
predict_basic_train <- rmse(data$c_mlu, predict_basic_train)
predict_basic_train

#0.3444659



#predict best model
predict_best_train <- predict(mlu_model_3, data)

#testing performance of best model on trainging data 
predict_best_train <- rmse(data$c_mlu, predict_best_train)
predict_best_train

#0.3445293

```


```{r}

#We test the performance of the models on the test data

test_actual <- data_test$c_mlu


#Basic model
predict_best_test <- rmse(predict_basic_train, test_actual)
#0.9098694



#Best model
predict_best_test <- rmse(predict_best_train, test_actual)
#0.909806


```

[HERE GOES YOUR ANSWER]

### Exercise 2) Model Selection via Cross-validation (N.B: ChildMLU!)

One way to reduce bad surprises when testing a model on new data is to train the model via cross-validation. 

In this exercise you have to use cross-validation to calculate the predictive error of your models and use this predictive error to select the best possible model.

- Use cross-validation to compare your model from last week with the basic model (Child MLU as a function of Time and Diagnosis, and don't forget the random effects!)
- (Tips): google the function "createFolds";  loop through each fold, train both models on the other folds and test them on the fold)

Which model is better at predicting new data: the one you selected last week or the one chosen via cross-validation this week?

- Test both of them on the test data.
- Report the results and comment on them.

- Now try to find the best possible predictive model of ChildMLU, that is, the one that produces the best cross-validated results.

NOTES WITH RICCARDO: 

#devtools::install_github("LudvigOlsen/groupdata2")
#devtools::install_github("LudvigOlsen/cvms")

#https://github.com/LudvigOlsen/cvms

#library(cvms)
#library(groupdata2)

#data <- fold(data, k = 5,
#             cat_col = 'gender',
#             id_col = 'subject') %>% 
#  arrange(.folds)

#CV1 <- cross_validate(data, "c_mlu~Age+gender+(1+visit|subject)", 
#                     folds_col = '.folds', 
#                     family='gaussian', 
#                     REML = FALSE)


```{r}
#BASIC MODEL

#Unique makes sure that the same child is divided into the same fold all the time
folds <- createFolds(unique(data$subject),k=5) #splitting data based on subjects, we want to avoid leacage, add list = T????

n = 1
rmse_train = NULL
rmse_test_basic = NULL

for(f in folds){
  train_set = subset(data,!(subject %in% f))
  test_set = subset(data,(subject %in% f))
  mlu_basic = lmer(c_mlu~Age+gender+(1+visit|subject), train_set, REML = F) #we fit
  predict_basic_train <- predict(mlu_basic) #we predict (like we did earlier)
  predict_basic_test <- predict(mlu_basic, newdata = test_set, allow.new.levels=TRUE) #we use rmse (like earlier)
  rmse_train[n] = rmse(train_set$c_mlu,predict_basic_train)
  rmse_test_basic[n] = rmse(test_set$c_mlu,predict_basic_test)
  n = n+1} #1 as 5 different values, one for each fold 

list(rmse_test_basic) 
mean(rmse_test_basic)

```

```{r}
#BEST MODEL

folds1 <- createFolds(unique(data$subject),k=5)

n = 1
rmse_train = NULL
rmse_test_best = NULL

for(f in folds){
  train_set = subset(data,!(subject %in% f))
  test_set = subset(data,(subject %in% f))
  mlu_model_3 <- lmer(c_mlu~visit*diagnosis+Age+gender+(1+visit|subject), data, REML = F)
  predict_best_train <- predict(mlu_model_3)
  predict_best_test <- predict(mlu_model_3, newdata = test_set, allow.new.levels=TRUE) 
  rmse_train[n] = rmse(train_set$c_mlu,predict_best_train)
  rmse_test_best[n] = rmse(test_set$c_mlu, predict_best_test)
  n = n+1}


list(rmse_test_best) 
mean(rmse_test_best)




```

```{r}
## Looking for a better model - 1

folds1 <- createFolds(unique(data$subject),k=5)

n = 1
rmse_train = NULL
rmse_test_alt = NULL

for(f in folds){
  train_set = subset(data,!(subject %in% f))
  test_set = subset(data,(subject %in% f))
  mlu_model_alt <- lmer(c_mlu~visit*diagnosis+ados+(1+visit|subject), data, REML = F)
  predict_best_train <- predict(mlu_model_alt)
  predict_best_test <- predict(mlu_model_alt, newdata = test_set, allow.new.levels=TRUE) 
  rmse_train[n] = rmse(train_set$c_mlu,predict_best_train)
  rmse_test_alt[n] = rmse(test_set$c_mlu, predict_best_test)
  n = n+1}


list(rmse_test_alt) 
mean(rmse_test_alt)

## 2


folds1 <- createFolds(unique(data$subject),k=5)

n = 1
rmse_train = NULL
rmse_test_alt2 = NULL

for(f in folds){
  train_set = subset(data,!(subject %in% f))
  test_set = subset(data,(subject %in% f))
  mlu_model_alt2 <- lmer(c_mlu~visit*diagnosis+Ethnicity+(1+visit|subject), data, REML = F)
  predict_best_train <- predict(mlu_model_alt2)
  predict_best_test <- predict(mlu_model_alt2, newdata = test_set, allow.new.levels=TRUE) 
  rmse_train[n] = rmse(train_set$c_mlu,predict_best_train)
  rmse_test_alt2[n] = rmse(test_set$c_mlu, predict_best_test)
  n = n+1}


list(rmse_test_alt2) 
mean(rmse_test_alt2)



## 3 


folds1 <- createFolds(unique(data$subject),k=5)

n = 1
rmse_train = NULL
rmse_test_alt3 = NULL

for(f in folds){
  train_set = subset(data,!(subject %in% f))
  test_set = subset(data,(subject %in% f))
  mlu_model_alt3 <- lmer(c_mlu~visit*diagnosis+ados+Ethnicity+(1+visit|subject), data, REML = F)
  predict_best_train <- predict(mlu_model_alt3)
  predict_best_test <- predict(mlu_model_alt3, newdata = test_set, allow.new.levels=TRUE) 
  rmse_train[n] = rmse(train_set$c_mlu,predict_best_train)
  rmse_test_alt3[n] = rmse(test_set$c_mlu, predict_best_test)
  n = n+1}


list(rmse_test_alt3) 
mean(rmse_test_alt3)


### has the lowest mean rmse value 
```

[HERE GOES YOUR ANSWER]


### Exercise 3) Assessing the single child

Let's get to business. This new kiddo - Bernie - has entered your clinic. This child has to be assessed according to his group's average and his expected development.

Bernie is one of the six kids in the test dataset, so make sure to extract that child alone for the following analysis.



You want to evaluate:

- how does the child fare in ChildMLU compared to the average TD child at each visit? Define the distance in terms of absolute difference between this Child and the average TD.
(Tip: recreate the equation of the model: 
Y=Intercept+Beta*X1+Beta*X2, etc; input the average of the TD group  for each parameter in the model as X1, X2, etc.).

To get average prediction of the model: 
avg = intercept + data$visit * b_visit (coefficients)
    = 0.5       + 1 (visit 1)* 0.3 = 0.8 
    = 0.5       + 2 (visit 2)* 0.3 = 1.1 etc


Including an interaction effect: 
avg = intercept + data$visit * b_visit + 
      data$diagnosis * b_diagnosis + 
      data$diagnosis * data_visit * b_interaction 



- how does the child fare compared to the model predictions at Visit 6? Is the child below or above expectations? (tip: use the predict() function on Bernie's data only and compare the prediction with the actual performance of the child)

```{r}

#filtering bernie out and finding the mean
bernie <- data_test %>% 
  filter(subject==2) 
  
bernie <- dplyr::select(bernie, visit, subject, diagnosis, c_mlu)

#Creating a data set with TD from training
TD_train <- filter(data, diagnosis == "TD")

#Data set only with visit, subject and c_mlu from training
TD_clean_train <- dplyr::select(TD_train, visit, subject, diagnosis, c_mlu)


#Creating a data set with TD from test
TD_test <- filter(data_test, diagnosis == "TD")

#Data set only with visit, subject and c_mlu from test
TD_clean_test <- dplyr::select(TD_test, visit, subject, diagnosis, c_mlu)

#Combining all the TD's across test/train
TD_all <- rbind(TD_clean_train, TD_clean_test)


#Bernie and TD_train
TD_and_B <- rbind(TD_all, bernie)


#ggplot
ggplot(TD_and_B, aes(x = visit, y = c_mlu, color=diagnosis))+
   geom_point()+
  geom_smooth(method="lm")+
  labs(title="Figure 1: Bernie and all TD children")

#subtracting TD-means from Bernie for each visit
Visit1 <- subset(TD_all, visit == "1")
1.984456-mean(Visit1$c_mlu)
# 0.6726901

Visit2 <- subset(TD_all, visit == "2")
2.544444-mean(Visit2$c_mlu)
#0.768709

Visit3 <- subset(TD_all, visit == "3")
3.353191-mean(Visit3$c_mlu)
#1.126587

Visit4 <- subset(TD_all, visit == "4")
3.183099-mean(Visit4$c_mlu)
#0.4517917

Visit5 <- subset(TD_all, visit == "5")
3.173252-mean(Visit5$c_mlu)
#0.1807251

Visit6 <- subset(TD_all, visit == "6")
3.448413-mean(Visit6$c_mlu)
#0.5374253

#Liniar model
TD_model <- lm(c_mlu ~ visit , TD_all)
summary(TD_model)

bernie_pre <- predict(TD_model, newdata = bernie, allow.new.levels = TRUE)
bernie_pre

#predict= 2.857214
#actual = 3.448413
#Bernie is bove expectations 

```


[HERE GOES YOUR ANSWER]
