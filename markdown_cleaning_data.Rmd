```{r}
#libraries for cleaning up the data and creating a new dataset - the test dataset

library(tidyverse)
library(lme4)
library(Metrics)
library(dplyr)
library(stringr)
```


```{r}
#loading data created in assignment 1

data <- read.csv("1.final_language_data.csv",header=T, sep=",")


#new datasets

demo_test <- read.csv("demo_test.csv",header=T, sep=",")

ul_test <- read.csv("LU_test.csv" ,header=T, sep=",")

toke_test <- read.csv("token_test.csv",header=T, sep=",")
```


```{r}
# cleaning up the test data sets into one test data set
# all copied from assigment part 1 


# We transform variable names in each dataset using dplyr (within tidyverse)

#Demographic data for the participants:
demo_test <- dplyr::rename(demo_test, visit=Visit)

demo_test <- dplyr::rename(demo_test, subject=Child.ID, nonVerbalIQ=MullenRaw, verbalIQ=ExpressiveLangRaw)

#Word data:
token_test<-dplyr::rename(toke_test, subject=SUBJ, visit=VISIT, m_uniqw=types_MOT, c_uniqw=types_CHI, shared_uniqw=types_shared, c_words=tokens_CHI, m_words=tokens_MOT)

#Deleting the last and unnecessary column called X

token_test<-select(token_test,-X)

#Length of utterance data:
utter_test<-dplyr::rename(ul_test, subject=SUBJ,visit=VISIT, m_mlu=MOT_MLU, m_lu_std=MOT_LUstd, m_lu_q1=MOT_LU_q1,m_lu_q2=MOT_LU_q2,m_lu_q3=MOT_LU_q3,c_mlu=CHI_MLU,c_lu_std=CHI_LUstd,c_lu_q1=CHI_LU_q1,c_lu_q2=CHI_LU_q2,c_lu_q3=CHI_LU_q3)




utter_test$visit <- str_extract(utter_test$visit, "[0-9]+")  

token_test$visit <- str_extract(token_test$visit, "[0-9]+")




utter_test$subject <- gsub("\\.", "", utter_test$subject)

token_test$subject <- gsub("\\.", "", token_test$subject)

demo_test$subject <- gsub("\\.", "", demo_test$subject)




demo_new1 <- select(demo_test, c(subject, visit, Ethnicity, Diagnosis, Gender, Age, ADOS, nonVerbalIQ, verbalIQ))

utter_new1 <- select(utter_test, c(subject, visit, m_mlu, m_lu_std, c_mlu, c_lu_std))

token_new1 <- select(token_test, c(subject, visit, m_uniqw, c_uniqw, m_words, c_words))




token_test <- plyr::join(token_new1, utter_new1)  


lan_data1 <- plyr::join(token_test, demo_new1, type="full")  





subset<-lan_data1 %>%
  filter(visit==1) %>%
  select(subject,ADOS)

#We create new variable names 
subset1 <- dplyr::rename(subset, ados=ADOS)

# We merge subset to lan_data 
final_data_test<- merge(lan_data1,subset1)

#We select which columns to keep and define the order of values
final_data_test <-select(final_data_test,subject,visit,Diagnosis,Age,Gender,Ethnicity,ados,c_uniqw,m_uniqw,c_words,m_words,c_mlu,m_mlu,c_lu_std,m_lu_std)




final_data_test<-final_data_test %>%
  mutate(subject = as.numeric(as.factor(subject)))

#We change the values "1" and "2" within the gender column into "F" and "M"
final_data_test <- final_data_test %>%
mutate(gender = ifelse(Gender %in% c("1"), "M", "F"))

#We delete the old column 
final_data_test$Gender <- NULL #deletes the old column

#We change the values "1" and "2" within the diagnosis column into "ASD" and "TD"
final_data_test <- final_data_test %>%
mutate(diagnosis = ifelse(Diagnosis %in% c("A"), "ASD", "TD"))

#We place the columns in the right order
final_data_test <-select(final_data_test,subject,visit,diagnosis,Age,gender,Ethnicity,ados,c_uniqw,m_uniqw,c_words,m_words,c_mlu,m_mlu,c_lu_std,m_lu_std)


#We write the data into a csv file
write.csv(final_data_test, file = "language_data_test.csv")
```

